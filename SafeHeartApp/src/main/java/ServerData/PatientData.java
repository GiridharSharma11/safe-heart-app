/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerData;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class PatientData {

    // This variable is initialized to be used by all the methods in this class avodiding the 
    // need to re initialize the object server times
    private final ServerConnection dataReports = new ServerConnection();
    
    /**
     * This function returns all the cholesterol values of a patient by taking
     * in a parameter of a patient ID
     *
     * @param patientID
     * @return HashMap of the cholesterol values and the date it was measured at
     * @throws Exception
     */
    public HashMap<Double, String> getTotalCholestrol(String patientID) throws Exception {

        // A new HashMap is created to store the cholesterol values and dates it was taken at
        HashMap<Double, String> cholestrolLevels = new HashMap<>();

        // Data from the server is retrieved specifically for a particular patient, all the data being retrieved
        // is related to the total chcolesterol level of a patient
        final JSONObject ReportList = dataReports.getJSONData("Observation?code=http://loinc.org%7C2093-3&_count=50&patient=" + patientID);
        // All the reports are returned
        if (ReportList.has("entry")){
            final JSONArray cholestrolArray = ReportList.getJSONArray("entry");
            final int numberOfReports = cholestrolArray.length();
            // This variable is created to store the dates the cholesterol value corresponds to
            String dateTime;
            // All the reports are looped through
            for (int i = 0; i < numberOfReports; i++) {
                
                final JSONObject validityCheck = cholestrolArray.getJSONObject(i).getJSONObject("resource");
                // This check is made to ensure that fields that are being parsed in the JSONObject do exist
                // to avoid errors
                if (validityCheck.has("issued") && validityCheck.has("valueQuantity")){
                    dateTime = validityCheck.getString("issued");
                    // The date of when the cholesterol was measured is retrieved
                    dateTime = dateTime.substring(0, (dateTime.lastIndexOf("T")));
                    // The cholesterol value is retrieved
                    double cholestrolLevel = validityCheck.getJSONObject("valueQuantity").getDouble("value");
                    cholestrolLevels.put(cholestrolLevel, dateTime);
                }
            }
        }
        return cholestrolLevels;
    }

    /**
     * This function receives data from the FHIR server for an individual patient and 
     * parses it to get all the blood pressure values (diastolic and systolic) of a patient from different issued dates
     * and return them in a HashMap form
     * @param patientID : The ID of a patient
     * @return LinkedHashMap<String, ArrayList< Double >> the blood pressure values in a double format with the date they were measured at
     * @throws java.lang.Exception
     */
    public LinkedHashMap<String, ArrayList<Double>> getBloodPressure(String patientID) throws Exception {

        // A new LinkedHashMap is created to store the blood pressure values and dates it was taken at
        LinkedHashMap<String, ArrayList<Double>> bloodPressure = new LinkedHashMap<>();
        
        // The following data is retrieved specific to one patient, listing down all the data related to a patients
        // blood pressure values, diastolic and systolic
        final JSONObject ReportList = dataReports.getJSONData("Observation?code=http://loinc.org%7C55284-4&_count=50&_sort=date&patient="+ patientID);
        // If the report does consist of an entry, meaning there are blood pressure values to be retrieved
        // The follow check ensures that and further parsing is carried out
        if (ReportList.has("entry")){
            // The JSONArray which corresponds to all the blood pressure values of a singular patient are sotred 
            // in the variable bloodpressureReports
            final JSONArray bloodpressureReports = ReportList.getJSONArray("entry");
            // This variable is used to store the data a blood pressure was retrieved at
            String dateTime;
            // The number of reports present are counted to loop through all for getting blood pressure records
            final int numberOfReports = bloodpressureReports.length();
        
            // All the reports are looped through
            for (int i = 0; i < numberOfReports; i++) {
                // This ArrayList is initialized in every loop to get rid of duplicated values being added to the 
                // list which is storing the diastolic and systolic values (diastolic is the first value and systolic is the secodn value)
                ArrayList<Double> bloodLevels = new ArrayList<>();
                JSONObject dataResource = bloodpressureReports.getJSONObject(i).getJSONObject("resource");
                JSONArray BPValuesArray = dataResource.getJSONArray("component");
                // This check is made to ensure that only the fileds which are present in the JSON object will be parsed
                // to eliminate any exception errors
                if (dataResource.has("issued") && 
                        BPValuesArray.getJSONObject(0).has("valueQuantity") && BPValuesArray.getJSONObject(1).has("valueQuantity")){
                    // Dates and the blood pressure diastolic and systolic are retrieved and stored into the respective variables
                    dateTime = dataResource.getString("issued");
                    dateTime = dateTime.substring(0, dateTime.lastIndexOf("T"));
                    Double diastolic = BPValuesArray.getJSONObject(0).getJSONObject("valueQuantity").getDouble("value");
                    Double systolic = BPValuesArray.getJSONObject(1).getJSONObject("valueQuantity").getDouble("value");
                    bloodLevels.add(diastolic);
                    bloodLevels.add(systolic);
                    bloodPressure.put(dateTime, bloodLevels);
                }              
            }
        }   
        return bloodPressure;
    }
    
    /**
     * This functions examines data received from the FHIR server and parses the JSON data to find the latest
     * tobacco use status of a particular patient
     * @param patientID : The ID number of a patient
     * @return : The latest tobacco use status of a patient
     * @throws Exception 
     */
    public String getTobaccoUse(String patientID) throws Exception {
        
        String tobaccoUse = null;

        // Data from the FHIR server is received after connections being made for a specific patient by defining 
        // parameters limiting to getting one and the latest data for a patient
        JSONObject ReportList = dataReports.getJSONData("Observation?code=http://loinc.org%7C72166-2&_count=1&_sort=-date&patient=" + patientID);
        // This check is made to ensure that present fields in teh JSON object are being parsed
        // eliminating errors
        if (ReportList.has("entry")){
            JSONArray tobaccoReport = ReportList.getJSONArray("entry");
            tobaccoUse = tobaccoReport.getJSONObject(0).getJSONObject("resource").getJSONObject("valueCodeableConcept").getString("text");
        } 
        return tobaccoUse;
   }

    

    /**
     * This function parses the JSON data retrieved from the FHIR server and finds the
     * age of a patient in years, months and day
     * @param patientID : ID of a patient
     * @return String age : the age of the patient
     * @throws Exception
     */
    public String getAge(String patientID) throws Exception {

        // Vaiables which retrieve data from the server are initialized
        final JSONObject ReportList = dataReports.getJSONData("Patient/" + patientID);
        final String birthDate = ReportList.getString("birthDate");

        // The date is broken down into year, month and date from determining the age of the patient
        final int year = Integer.valueOf(birthDate.substring(0, 4));
        final int month = Integer.valueOf(birthDate.substring(5, 7));
        final int day = Integer.valueOf(birthDate.substring(8, 10));
        // The date of birth is specified by passing in the respective values
        LocalDate dateofBirth = LocalDate.of(year, month, day);
        // The current date is retrieved
        LocalDate currentDate = LocalDate.now();
        // The difference between the two dates is measured to get the age of the patient
        Period diff = Period.between(dateofBirth, currentDate);
        // Stores the accurate entire date into a string and is returned
        String age = diff.getYears() + " years " + diff.getMonths() + " months " + diff.getDays() + " days";
        return age;

    }
    
     /**
     * This function looks up at the data retrieved from the FHIR server, and parses the
     * JSON data to find a patients gender and return it
     * @param PatientID : the ID of a patient
     * @return : The gender of the patient
     * @throws Exception 
     */
    public String getGender(String PatientID) throws Exception {
        
        String gender = null;
        ServerConnection patientData = new ServerConnection();
        JSONObject PatientRecord = patientData.getJSONData("Patient/" + PatientID);
        // This check is made to make sure the data being parsed actually is presrnt in the JSONObject
        // this is done to eliminate exception errors
        if (PatientRecord.has("gender")){
            gender = PatientRecord.getString("gender");
        }
        
        return gender;

    }
    
    /**
     * This function checks if a patient is under hypertensive crisis
     * @param diastolicBP : the latest value of the diastolic blood pressure
     * @param systolicBP : the latest value of the systolic blood pressure
     * @return : isHypertensive a Boolean, true if the patient is under hypertensive crisis
     * false otherwise
     * @throws ParseException 
     */
    public Boolean hypertensiveCheck(Double diastolicBP, Double systolicBP) throws ParseException{
        // The threshold values to determine if a patient is under hypertensive crisis are declared
        final double diastolicLevel = 120.00;
        final double systolicLevel = 180.00;
        // A check for the hypertensive crisis is initialized as a boolean
        Boolean isHypertensive = false;

        // If the latest blood pressure (diastolic or sysotlic) does exceed the threshold
        // the patient is found to be under hypertensive crisis
        if (diastolicBP > diastolicLevel || systolicBP > systolicLevel){      
            isHypertensive = true;
        }     
        return  isHypertensive;
    }

}

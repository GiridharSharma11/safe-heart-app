/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerData;

import org.json.JSONObject;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class UserData {
    
    /**
     * This function returns the full name of a user, either a practitioner or a patient, this can be specified by
     * one of the parameters, data is parsed and the three parts of the name are concatenated to returns a full 
     * name
     * @param reference: either a patient or a practitioner whose name is to be retrieved
     * @param userID: ID of the user
     * @return finalName : the final name of the user
     * @throws Exception 
     */
    public String getUserName(String reference, String userID) throws Exception{

       String finalName = null;
       final String extensionURL = reference + "/" + userID;
       // A connection to the server is made to retrieve JSON data
       final ServerConnection urlData = new ServerConnection();

       // Parsing data specific to a particular patient
       JSONObject practitionerRecord = urlData.getJSONData(extensionURL);
       final JSONObject validityCheck = practitionerRecord.getJSONArray("name").getJSONObject(0);
       
       // This check is made to ensure that an empty JSONObject shall not be parsed to eliminate
       // exception errors
       if (validityCheck.has("given") && validityCheck.has("prefix") && validityCheck.has("family")){
           String firstName = validityCheck.getJSONArray("given").getString(0);
           String title = validityCheck.getJSONArray("prefix").getString(0);
           String lastName = validityCheck.getString("family");
           finalName = title + " " + firstName + " " + lastName;
       }  
       return finalName;
    }


    
}

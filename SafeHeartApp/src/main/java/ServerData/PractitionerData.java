/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ServerData;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class PractitionerData {

    /**
     * This function takes in a practitioner ID and returns all their patients in a ArrayList
     * @param practitionerID : The ID of the practitioner whose patients are to be found and retrieved
     * @return ArrayList< String > list of patients of the practitioner
     * @throws Exception 
     */
    public ArrayList<String> patientList(String practitionerID) throws Exception{
        
        // Initializing all the variables and objects
        final ServerConnection JSONData = new ServerConnection();
        // Getting the data from the FHIR server specific to the particular practitioner, lists down all encounters to 
        // parse all the patients a clinician has
        final JSONObject data = JSONData.getJSONData("Encounter?_include=Encounter%3Apatient&_count=1000&_pretty=true&practitioner="+practitionerID);
        final JSONArray encounterArray = data.getJSONArray("entry");
        final int numberOfReports = encounterArray.length();
        String PatientID;
        ArrayList<String> patientArray = new ArrayList<>();
        
        // This loop runs through all the encounters for a particular clinician
        for (int i=0; i < numberOfReports; ++i){
            // JSONObject variable initialized at the beginning to avoid code repetation
            final JSONObject subjectCheck = encounterArray.getJSONObject(i).getJSONObject("resource");
            
            // This check is retrieved to ensure that the field which is being parsed actually exists in JSONObject
            if (subjectCheck.has("subject")){
                // If it exists in the JSONObject then the reference ID of a patient is retrieved from the encounter
                PatientID = subjectCheck.getJSONObject("subject").getString("reference");
                // The ID is scraped off to return an all numeric value
                PatientID = PatientID.substring((PatientID.lastIndexOf("/"))+1, PatientID.length());
                
                // This check is made to see if a patient ID is not being repeatedly added to the same list
                // avoiding redudancy
                if (!patientArray.contains(PatientID)){
                    patientArray.add(PatientID);
                }
            }
        }    
        return patientArray;
    }
    
}

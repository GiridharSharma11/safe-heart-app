package ServerData;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

/*
 * This class is responsible for connecting to the FHIR server and retrieving the data from the server
 */

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal
 */
public class ServerConnection {
    
    // The String variable URL stores the initial URL without the extension 
    // to be passed into a function to access data from the server
    
    private final static String URL= "http://hapi-fhir.erc.monash.edu:8080/baseDstu3/";
      
    /**
     * 
     * @param extension, which specifies which data to retrieve from the FHIR server
     * @return serverData, the data in JSON format retrieved from the FHIR server
     * @throws Exception if an error arises while retrieving data from the FHIR server sometimes in the case of VPN disconnection
     */
    public JSONObject getJSONData(String extension) throws Exception{
        
        //specific url to fetch the data from
        String url = URL + extension; 
        //A URL object is created
        URL obj = new URL(url); 
        //HTTP connection is opened to retrieve data from the server
        HttpURLConnection con = (HttpURLConnection) obj.openConnection(); 
        // Since we only want to retrieve data from the server, the request method is set to GET
        con.setRequestMethod("GET"); //use the GET method to receive the data
        // Data is requested in the from of JSON
        con.setRequestProperty("Accept", "application/json");
       
        StringBuffer response;
        
        // A stream reader object is created to read all the data retrieved
        try (BufferedReader dataInput = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = dataInput.readLine()) != null) {
                // The entire response is appended into the buffer
                response.append("\n").append(inputLine); 
            }
        }
        // The reponse retrieved from the server is converted into a JSONObject type
        JSONObject serverData = new JSONObject(response.toString()); //convert the response string to a JSOn object
        
        // The JSONObject is returned for use
        return serverData;
    }

    

}

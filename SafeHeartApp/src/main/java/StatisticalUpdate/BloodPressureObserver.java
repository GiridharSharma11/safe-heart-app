/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

import Entities.Patient;
import ServerData.PatientData;
import UserInterface.MonitorScreen;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddhant Dube
 */
public class BloodPressureObserver extends Observer {
    
    /**
     * Initializing class variable instances
     * @param subject : the subject (observable)
     * @param monitor : The monitor screen of a particular patient
     * @param patient : the instance of a patient
     */
    public BloodPressureObserver(PatientVitalsSubject subject, MonitorScreen monitor, Patient patient){
        // The subject is referenced to the variable defined 
        this.subject = subject;
        // The monitor screen which is passed into the constructor is referenced to the variable defined
        this.monitor = monitor;
        // The patient which is passed into the constructor is referenced to the variable defined
        this.patient = patient;
        // This registers an observer in the list of observers being monitored
        this.subject.register(this);

    }
    
    
    @Override
    public void update() {
        // new instance of PatientData is created to retrieve the blood pressure levels for a patient
        PatientData bloodPressureDate = new PatientData();
        try {
            // The blood pressure levels along with their dates are retrieved
            LinkedHashMap<String, ArrayList<Double>> bloodLevels = bloodPressureDate.getBloodPressure(this.patient.getuserID());
            // The new blood pressure levels are replaced with the older ones in the monitor class
            this.monitor.updatePatientBloodLevel(bloodLevels);
            
        } catch (Exception ex) {
            Logger.getLogger(CholestrolObserver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

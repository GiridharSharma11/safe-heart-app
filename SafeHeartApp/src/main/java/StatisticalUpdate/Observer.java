/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

import Entities.Patient;
import UserInterface.MonitorScreen;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public abstract class Observer {
    
    // Instances of a subject, monitor and patient to be used for referencing to a particular observer
    // for data updation
    protected PatientVitalsSubject subject;
    protected MonitorScreen monitor;
    protected Patient patient;
    /**
     * update method to update the observers
     */
    public abstract void update();
    

}

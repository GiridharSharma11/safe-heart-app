/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class PatientVitalsSubject implements Subject {
    
    // This array list will manage the storing of observers being registered and de-registered
    private final List<Observer> temporaryObservers = new ArrayList<>();
    
    // Timer class instance is created to update observers every certain time
    private final Timer timer = new Timer();
    
    /**
     * This function initiates the timer which runs every hour as specified in the scheduleAtFixedRate
     * every hour this function call the notifyObserver function to signal all the registered observers for
     * any new data from the server
     */ 
    public void startTimer(){
   
        TimerTask task = new TimerTask(){
            @Override
            public void run() {
                // this is the function which is to be run every one hour
                notifyObserver();
            }
        };
        // This determines after how long intervals will the function run
        timer.scheduleAtFixedRate(task, 0, 3600000);
    }
        
    /**
     * Observers are added into a temporary array list to get notified about any changes time to time
     * @param observer 
     */
    @Override
    public void register(Observer observer){
        temporaryObservers.add(observer);
    }
    
    /**
     * This function notifies all the registered observer with any new data
     */
    @Override
    public void notifyObserver(){
    
        // This set is responsible to getting rid of duplicated values being added
        Set<Observer> uniqueObservers = new HashSet<>();
        uniqueObservers.addAll(temporaryObservers);
        // Before the observers list starts iterating, the latest observers are set
        // into the observers list, this is to eliminate the concurrent modification exception
        List<Observer> observers = new ArrayList<>(uniqueObservers);
       
        // All registered observers are notified for latest new data from the FHIR server
        observers.forEach((observer) -> {
            observer.update();
        });
    
    }

    /**
     * Observers are removed from a temporary array list to stop getting notified for new updates
     * @param observer 
     */
    @Override
    public void deregister(Observer observer) {
        temporaryObservers.remove(observer);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

import Entities.Patient;
import ServerData.PatientData;
import UserInterface.MonitorScreen;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddhant Dube
 */
public class TobaccoObserver extends Observer {

    /**
     * Initializing class variable instances
     * @param subject
     * @param monitor
     * @param patient
     */
//    private final MonitorScreen monitor;
//    private final Patient patient;
    
    public TobaccoObserver(PatientVitalsSubject subject, MonitorScreen monitor, Patient patient){
        // The subject is referenced to the variable defined 
        this.subject = subject;
        // The monitor screen which is passed into the constructor is referenced to the variable defined
        this.monitor = monitor;
        // The patient which is passed into the constructor is referenced to the variable defined
        this.patient = patient;
        // This registers an observer in the list of observers being monitored
        this.subject.register(this);

    }
    
    @Override
    public void update() {
        // new instance of PatientData is created to retrieve the latest tobacco status for a patient
        PatientData tobaccoData = new PatientData();
        try {
            // The latest tobacco status for a particular patient is retrieved
            String tobaccoUsage = tobaccoData.getTobaccoUse(this.patient.getuserID());
            // The new tobacco status is replaced with the older one in the monitor class
            this.monitor.updatePatientTobacco(tobaccoUsage);
            
        } catch (Exception ex) {
            Logger.getLogger(CholestrolObserver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

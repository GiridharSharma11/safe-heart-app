/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

/**
 *
 * @author Siddhant Dube
 */
public interface Subject {
    /**
     * This method is responsible to register an observer for it to be able to constantly update
     * @param observer 
     */
    public void register(Observer observer);
    
    /**
     * This method is responsible to de-register an observer to stop receiving new updates
     * @param observer 
     */
    public void deregister(Observer observer);
    
    /**
     * This method ensures all the attached (registers) observers are notified for new data every
     * specific amount of time
     */
    public void notifyObserver();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StatisticalUpdate;

import Entities.Patient;
import ServerData.PatientData;
import UserInterface.MonitorScreen;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class CholestrolObserver extends Observer {
    
    /**
     * This constructor registers the CholestrolObserver as an observer once initialized anywhere
     * @param subject
     * @param monitor
     * @param patient 
     */
    @SuppressWarnings("LeakingThisInConstructor")
    public CholestrolObserver(PatientVitalsSubject subject, MonitorScreen monitor, Patient patient){
        // The subject is referenced to the variable defined 
        this.subject = subject;
        // The monitor screen which is passed into the constructor is referenced to the variable defined
        this.monitor = monitor;
        // The patient which is passed into the constructor is referenced to the variable defined
        this.patient = patient;
        // This registers an observer in the list of observers being monitored
        this.subject.register(this);

    }
    
    /**
     * This method updates the monitor screen by checking the FHIR server again and retrieving new
     * cholesterol values and updating them in the monitor screen j-frame
     */
    @Override
    public void update() {
        // new instance of PatientData is created to retrieve the new cholesterol levels for a patient
        PatientData cholestrolData = new PatientData();
        try {
            // The new cholesterol levels are retrieved along with their dates
            HashMap<Double, String> cholestrolLevels = cholestrolData.getTotalCholestrol(this.patient.getuserID());
            // The new cholesterol levels are replaced with the older ones in the monitor class
            this.monitor.updatePatientCholestrol(cholestrolLevels);
            
        } catch (Exception ex) {
            Logger.getLogger(CholestrolObserver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }    
}   

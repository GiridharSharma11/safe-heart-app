/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.safeheartapp;

import StatisticalUpdate.PatientVitalsSubject;
import UserInterface.LoginPage;


/**
 *
 * @author giridhargopalsharma
 * @author Siddhant Dube
 */
public class Main {
    
    /**
     * This main method is the starting point of the entire application
     * @param args
     * @throws Exception 
     */
    public static void main(String[] args) throws Exception {
        // An instance of subject is created which is the observant
        PatientVitalsSubject subject = new PatientVitalsSubject();
        // Timer starts to keep checking the FHIR server every 60 minutes (1 hour)
        subject.startTimer();
        
        // The login page is created and displayed
        LoginPage frameware = new LoginPage(subject);
        frameware.setVisible(true);
    }
    
    
    
}

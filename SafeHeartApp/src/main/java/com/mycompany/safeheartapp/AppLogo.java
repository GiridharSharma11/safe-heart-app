/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.safeheartapp;

import java.awt.Toolkit;
import javax.swing.JFrame;

/**
 *
 * @author Siddhant Dube
 */
public class AppLogo {
    
    /**
     * This class is responsible for embedding an image as the application logo to every frame of the app
     * @param frame : the particular frame where the logo is to be embedded
     */
    public void setAppIcon(JFrame frame) {
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/SafeHeartMainLogo.png")));        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Entities.Patient;
import StatisticalUpdate.BloodPressureObserver;
import StatisticalUpdate.CholestrolObserver;
import StatisticalUpdate.PatientVitalsSubject;
import StatisticalUpdate.TobaccoObserver;
import ServerData.PatientData;
import com.mycompany.safeheartapp.AppLogo;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

/**
 *
 * @author giridhargopalsharma
 * @author Siddhant Dube
 */
public class MonitorScreen extends javax.swing.JFrame {

    /**
     * Creates new form MonitorScreen
     * @param patient
     */

    // subject instance passed from the homepage
    private final PatientVitalsSubject passedSubject;
    
    public MonitorScreen(Patient patient, PatientVitalsSubject subject) throws ParseException {
        initComponents();
        // The application logo is set as the frame logo
        AppLogo logo = new AppLogo();
        logo.setAppIcon(this);
        this.passedSubject = subject;
        this.patient = patient;
        // This method is called to display all the patients personal info
        setPatientInfo(patient);       
        // The methods are called to make sure that data to be monitored is actually present
        // if it is not the practitioner will not be able to monitor any unavailable data
        BPMonitorData();
        CholesterolMonitorData();   
        checkboxValidity();
    }
    
    /**
     * This method gets all the patient personal details and sets them to the labels in the
     * frame
     * @param patient 
     */
    private void setPatientInfo(Patient patient){
        
        this.patientname = patient.getfullName();
        this.patientid = patient.getuserID();
        this.patientage = patient.getAge();
        this.patientgender = patient.getGender();
        this.patientcholestrol = patient.getCholestrol();
        this.patientTobacco = patient.getTobaccoUse();
        this.patientBloodPressure = patient.getBloodPressure();
        patientName.setText(patientname);
        patientID.setText(patientid);
        patientAge.setText(patientage);
        patientGender.setText(patientgender);
    }
    
    /**
     * This method checks for the availability for the patients vitals,
     * if a data is not present then the check box is greyed out to 
     * disallow a practitioner to choose that unavailable options to monitor
     */
    private void checkboxValidity(){
        
        if (this.patientBloodPressure.isEmpty()){
            bloodCheck.setEnabled(false);
        } 
        
        if (this.patientcholestrol.isEmpty()){
            cholesterolCheck.setEnabled(false);
        } 
        
        if (this.patientTobacco == null){
            tobaccoCheck.setEnabled(false);
        } 
    }

    /**
     * This function checks if a patient's blood pressure is being monitored, the latest 
     * blood pressure values are passed as an argument to a function which returns a Boolean to 
     * signal if a patient is under hypertensive crisis or not
     * @throws ParseException 
     */
    private void hypertensiveTest() throws ParseException{

        new java.util.Timer().schedule( 
            new java.util.TimerTask() {
            @Override
            public void run() {
                PatientData patientdata = new PatientData();
                try {
                    if (patientdata.hypertensiveCheck(diastolic.get(diastolic.size()-1), systolic.get(systolic.size()-1))){
                       JOptionPane.showMessageDialog(MonitorScreen.this, "Patient is showing signs of hypertensive crisis!", "Alert", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(MonitorScreen.class.getName()).log(Level.SEVERE, null, ex);
                }            
            }
            }, 
                2000 // This is to make sure the fucntions runs after a pause of 2 seconds to make the alert more realistic
        );
       
    }
    /**
     * This method is used for the patient cholesterol updating, the method is called
     * from the observer and the new set of data is set to the existing patient cholesterol parameter
     * and the monitor screen is updated
     * @param patientCholestrol 
     * @throws java.text.ParseException 
     */
    public void updatePatientCholestrol(HashMap<Double, String> patientCholestrol) throws ParseException{
        patientcholestrol = patientCholestrol;
        displayMonitorGraph();
    }
    
    /**
     * This method is used for the patient blood pressure updating, the method is called
     * from the observer and the new set of data is set to the existing patientBloodpressure parameter
     * and the monitor screen is updated
     * @param patientBP 
     * @throws java.text.ParseException 
     */
    public void updatePatientBloodLevel(LinkedHashMap<String, ArrayList<Double>> patientBP) throws ParseException{
        patientBloodPressure = patientBP;
        displayMonitorGraph();
    }
    
    /**
     * This method is used for the patient tobacco status updating, the method is called
     * from the observer and the new set of data is set to the existing patientTobacco parameter
     * and the monitor screen is updated
     * @param patientTobaccoUse 
     */
    public void updatePatientTobacco(String patientTobaccoUse){
        patientTobacco = patientTobaccoUse;
        if (tobaccoCheck.isSelected() == true){
            tobaccoMonitorData();
        }
    }
   
   
    /**
     * This function is responsible for setting the values of the patients
     * cholesterol values and the dates they were measured at, all the data
     * is set to the cholesterol TimeSeries which can be passed as an argument
     * in a JfreeChart variable for plotting purposes
     * @throws ParseException
     */
    private void CholesterolMonitorData() throws ParseException{
    
        // This is where the cholesterol levels is added to a new singular cholestrolValue ArrayList
        patientcholestrol.keySet().forEach((key) -> {
            cholestrolValue.add(key);
        });
        
        // This is where the date it was measured at was added to a datemeasured ArrayList
        patientcholestrol.values().forEach((value) -> {
            datemeasured.add(value);
        });
        // new TimeSeries is initialized which represents the patients cholesterol
        cholesterolSeries = new TimeSeries("Cholesterol (Mg/dL)");
        
        // All the items in the lists are added to the cholesterol series to be further plotted
        for (int i = 0; i < datemeasured.size(); i++){
            SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date mydate = standardDateFormat.parse(datemeasured.get(i));
            cholesterolSeries.addOrUpdate(new Day(mydate), cholestrolValue.get(i));  
        }
    
    }
    
    /**
     * This function is responsible for setting the values of the patients
     * blood pressure values and the dates they were measured at, all the data
     * is set to the blood pressure (diastolic and systolic) TimeSeries which can be passed as an argument
     * in a JfreeChart variable for plotting purposes
     * @throws ParseException
     */
    private void BPMonitorData() throws ParseException{

        // The dates from the HashMap are retrieved and set into a new arraylist
        patientBloodPressure.keySet().forEach((key) -> {
            BPDates.add(key);
        });
        
        // The blood pressure values (diastolic and systolic) are retrieved
        // and added into their respective array lists
        patientBloodPressure.values().forEach((value) -> {
            diastolic.add(value.get(0));
            systolic.add(value.get(1));
        });
        // time series are created
        diastolicSeries = new TimeSeries("Diastolic Blood Pressure (mmHg)");
        systolicSeries = new TimeSeries("Systolic Blood Pressure (mmHg)");
        // all values from the array list are added into the respective blood pressure series
        // for plotting
        for (int i = 0; i < BPDates.size(); i++){
            SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date mydate = standardDateFormat.parse(BPDates.get(i));
            diastolicSeries.addOrUpdate(new Day(mydate), diastolic.get(i));  
            systolicSeries.addOrUpdate(new Day(mydate), systolic.get(i));
        }
        
    }
    
    /**
     * This function sets the patient's tobacco status to a label in the monitor
     */
    private void tobaccoMonitorData(){
        tobaccoStatus.setText(patientTobacco);
    }
    
    /**
     * This function initializes the JFreeChart sets the features of the JFreeChart such as the border color,
     * the size of the border and the back ground color
     * @return : the XYPlot which is going to be plotted
     */
    private XYPlot setChartFeatures(){
        
        this.monitorChart.setBorderPaint(Color.WHITE);      
        XYPlot plot = monitorChart.getXYPlot();
        plot.setOutlinePaint(Color.YELLOW);
        plot.setOutlineStroke(new BasicStroke(2.0f));
        plot.setBackgroundPaint(Color.DARK_GRAY);       
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        return plot;
    
    }
    
    /**
     * This function is specifically responsible to visualize 
     * and display the graphs on a panel in the monitor JFrame
     */
    private void displayGraph(){
        ChartPanel barPanel = new ChartPanel(monitorChart);
        graphPanel.removeAll();
        graphPanel.add(barPanel, BorderLayout.CENTER);
        graphPanel.validate();
    }
    
    /**
     * This function determines how many vitals are to be plotted on a graph
     * and with respect to that, add them to the ChartFactory to be displayed.
     * The vitals can be 1, 2 or 3 depending on the parameter
     * @param seriesNumber 
     */
    private void initializeVitalGraph(int seriesNumber){
        ArrayList<TimeSeries> vitalSeries = new ArrayList<>();
        // This switch condition determines how many series are to be
        // plotted onto the graph panel, this is determines by the number of inputs
        switch (seriesNumber) {
            case 1:
                vitalSeries.add(cholesterolSeries);
                break;
            case 2:
                vitalSeries.add(diastolicSeries);
                vitalSeries.add(systolicSeries);
                break;
            case 3:
                vitalSeries.add(cholesterolSeries);
                vitalSeries.add(diastolicSeries);
                vitalSeries.add(systolicSeries);
                break;
            default:
                break;
        }
        // This loop will add the respective amount of series into the monitor collection,
        // depending on the number of vitals to be monitored
        TimeSeriesCollection MonitorCollection = new TimeSeriesCollection();
        for (int i = 0; i < seriesNumber; ++i){
            MonitorCollection.addSeries(vitalSeries.get(i));
        }
        // The JFreeChart is initialized with the TimeSeriesCollection and other attributes such as the
        // title, axis labels of the chart
        this.monitorChart = ChartFactory.createTimeSeriesChart("PATIENT VITALS", "Date Measured", "Value", MonitorCollection, true, true, false);
        XYPlot seriesplot = setChartFeatures();
        // This renderes the lines plotted on the graph
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        seriesplot.setRenderer(renderer);
        // This loop sets the size of the lines plotted in the graph
        for (int i = 0; i < seriesNumber; ++i){
            renderer.setSeriesStroke(i, new BasicStroke(2.0f));
        }
        // This method is called to visualize the graph
        displayGraph();
    }
    
    /**
     * This functions handles the un-registering of observers, to stop receiving new updated values
     * for any of the vitals unchecked from the check box
     */
    private void unregisterObservers(){
        
        if (bloodCheck.isSelected() == true){
            passedSubject.deregister(bloodPressureObserver);
        }
        if (cholesterolCheck.isSelected() == true){
            passedSubject.deregister(cholesterolObserver);
        }
        if (tobaccoCheck.isSelected() == true){
            passedSubject.deregister(tobaccoObserver);
        }
        
    }
    
    /**
     * This functions handles the registering of observers, to receive new updated values
     * for any of the vitals checked from the check box
     */
    private void registerObservers(){
        if (bloodCheck.isSelected() == true){
            bloodPressureObserver = new BloodPressureObserver(passedSubject, this, patient);
        }
        if (cholesterolCheck.isSelected() == true){
            cholesterolObserver = new CholestrolObserver(passedSubject, this, patient);
        }
        if (tobaccoCheck.isSelected() == true){
            tobaccoObserver = new TobaccoObserver(passedSubject, this, patient);
        }
    }

    /**
     * This functions checks which check boxes are ticked and based on that, display the respective 
     * data on the monitor screen, the check boxes can be checked in any combination for displaying
     * the vitals
     * @throws ParseException 
     */
    private void displayMonitorGraph() throws ParseException{
            if (bloodCheck.isSelected() == true && cholesterolCheck.isSelected() == false && tobaccoCheck.isSelected() == false){
                BPMonitorData();
                initializeVitalGraph(2);
                hypertensiveTest();
            }
            else if (bloodCheck.isSelected() == true && cholesterolCheck.isSelected() == true && tobaccoCheck.isSelected() == false){
                BPMonitorData();
                CholesterolMonitorData();  
                initializeVitalGraph(3);
                hypertensiveTest();
            }
            else if (bloodCheck.isSelected() == true && cholesterolCheck.isSelected() == true && tobaccoCheck.isSelected() == true){
                BPMonitorData();
                CholesterolMonitorData();  
                tobaccoMonitorData();
                initializeVitalGraph(3);
                hypertensiveTest();
            }
            else if (cholesterolCheck.isSelected() == true && bloodCheck.isSelected() == false && tobaccoCheck.isSelected() == false){
                CholesterolMonitorData();  
                initializeVitalGraph(1);

            }
            else if (cholesterolCheck.isSelected() == true && tobaccoCheck.isSelected() == true && bloodCheck.isSelected() == false){
                CholesterolMonitorData();  
                tobaccoMonitorData();
                initializeVitalGraph(1);
            }
            else if (bloodCheck.isSelected() == true && tobaccoCheck.isSelected() == true && cholesterolCheck.isSelected() == false){
                BPMonitorData();
                tobaccoMonitorData();
                initializeVitalGraph(2);
                hypertensiveTest();
            }
            else if (tobaccoCheck.isSelected() == true && bloodCheck.isSelected() == false && cholesterolCheck.isSelected() == false){
                tobaccoMonitorData();
            }
    }
        
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PatientInfo = new javax.swing.JPanel();
        nameLabel = new javax.swing.JLabel();
        ageLabel = new javax.swing.JLabel();
        IDLabel = new javax.swing.JLabel();
        GenderLabel = new javax.swing.JLabel();
        patientName = new javax.swing.JLabel();
        patientID = new javax.swing.JLabel();
        patientGender = new javax.swing.JLabel();
        patientAge = new javax.swing.JLabel();
        tobaccoStatusLabel = new javax.swing.JLabel();
        tobaccoStatus = new javax.swing.JLabel();
        graphPanel = new javax.swing.JPanel();
        MonitorOption = new javax.swing.JPanel();
        initiateMonitor = new javax.swing.JButton();
        closeMonitor = new javax.swing.JButton();
        bloodCheck = new javax.swing.JCheckBox();
        tobaccoCheck = new javax.swing.JCheckBox();
        cholesterolCheck = new javax.swing.JCheckBox();
        MonitorHeading = new javax.swing.JLabel();
        resetMonitor = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        PatientInfo.setBackground(new java.awt.Color(0, 0, 0));

        nameLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel.setText("Name :");

        ageLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        ageLabel.setForeground(new java.awt.Color(255, 255, 255));
        ageLabel.setText("Age :");

        IDLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        IDLabel.setForeground(new java.awt.Color(255, 255, 255));
        IDLabel.setText("ID :");

        GenderLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        GenderLabel.setForeground(new java.awt.Color(255, 255, 255));
        GenderLabel.setText("Gender :");

        patientName.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        patientName.setForeground(new java.awt.Color(255, 255, 255));
        patientName.setText("jLabel5");

        patientID.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        patientID.setForeground(new java.awt.Color(255, 255, 255));
        patientID.setText("jLabel7");

        patientGender.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        patientGender.setForeground(new java.awt.Color(255, 255, 255));
        patientGender.setText("jLabel8");

        patientAge.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        patientAge.setForeground(new java.awt.Color(255, 255, 255));
        patientAge.setText("jLabel6");

        tobaccoStatusLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tobaccoStatusLabel.setForeground(new java.awt.Color(255, 255, 255));
        tobaccoStatusLabel.setText("Tobacco Status :");

        tobaccoStatus.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tobaccoStatus.setForeground(new java.awt.Color(255, 255, 255));
        tobaccoStatus.setText("Unavailable");

        javax.swing.GroupLayout PatientInfoLayout = new javax.swing.GroupLayout(PatientInfo);
        PatientInfo.setLayout(PatientInfoLayout);
        PatientInfoLayout.setHorizontalGroup(
            PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PatientInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PatientInfoLayout.createSequentialGroup()
                        .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameLabel)
                            .addComponent(ageLabel)
                            .addComponent(IDLabel))
                        .addGap(79, 79, 79)
                        .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(patientName)
                            .addComponent(patientAge)
                            .addComponent(patientID)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PatientInfoLayout.createSequentialGroup()
                        .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tobaccoStatusLabel)
                            .addComponent(GenderLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(patientGender)
                            .addComponent(tobaccoStatus))))
                .addContainerGap(191, Short.MAX_VALUE))
        );
        PatientInfoLayout.setVerticalGroup(
            PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PatientInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLabel)
                    .addComponent(patientName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ageLabel)
                    .addComponent(patientAge))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(IDLabel)
                    .addComponent(patientID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(GenderLabel)
                    .addComponent(patientGender))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PatientInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tobaccoStatusLabel)
                    .addComponent(tobaccoStatus))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        graphPanel.setBackground(new java.awt.Color(204, 204, 204));
        graphPanel.setLayout(new java.awt.BorderLayout());

        MonitorOption.setBackground(new java.awt.Color(255, 51, 51));

        initiateMonitor.setText("Create Graph");
        initiateMonitor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initiateMonitorActionPerformed(evt);
            }
        });

        closeMonitor.setText("Stop Monitoring");
        closeMonitor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeMonitorActionPerformed(evt);
            }
        });

        bloodCheck.setBackground(new java.awt.Color(255, 255, 255));
        bloodCheck.setForeground(new java.awt.Color(0, 0, 0));
        bloodCheck.setText("Blood Pressure");
        bloodCheck.setBorder(null);
        bloodCheck.setOpaque(false);
        bloodCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bloodCheckActionPerformed(evt);
            }
        });

        tobaccoCheck.setForeground(new java.awt.Color(0, 0, 0));
        tobaccoCheck.setText("Tobacco");
        tobaccoCheck.setBorder(null);
        tobaccoCheck.setOpaque(false);
        tobaccoCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tobaccoCheckActionPerformed(evt);
            }
        });

        cholesterolCheck.setForeground(new java.awt.Color(0, 0, 0));
        cholesterolCheck.setText("Cholestrol");
        cholesterolCheck.setBorder(null);
        cholesterolCheck.setOpaque(false);
        cholesterolCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cholesterolCheckActionPerformed(evt);
            }
        });

        MonitorHeading.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        MonitorHeading.setForeground(new java.awt.Color(0, 0, 0));
        MonitorHeading.setText("TICK ONE OR MORE TO MONITOR");

        resetMonitor.setText("Reset Monitor");
        resetMonitor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetMonitorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout MonitorOptionLayout = new javax.swing.GroupLayout(MonitorOption);
        MonitorOption.setLayout(MonitorOptionLayout);
        MonitorOptionLayout.setHorizontalGroup(
            MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, MonitorOptionLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bloodCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(MonitorHeading)
                    .addComponent(tobaccoCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cholesterolCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addGroup(MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(closeMonitor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(resetMonitor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(initiateMonitor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(25, 25, 25))
        );
        MonitorOptionLayout.setVerticalGroup(
            MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(MonitorOptionLayout.createSequentialGroup()
                .addGroup(MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MonitorOptionLayout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(initiateMonitor))
                    .addGroup(MonitorOptionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(MonitorHeading, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(MonitorOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(MonitorOptionLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(bloodCheck, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(tobaccoCheck)
                        .addGap(18, 18, 18)
                        .addComponent(cholesterolCheck)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(MonitorOptionLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(resetMonitor)
                        .addGap(18, 18, 18)
                        .addComponent(closeMonitor)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PatientInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(MonitorOption, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(graphPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 922, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(MonitorOption, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PatientInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 419, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(0, 174, Short.MAX_VALUE)
                    .addComponent(graphPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void initiateMonitorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initiateMonitorActionPerformed
        try {
            
            // This check is made to see if no check boxes are checks then inform the practitioner 
            // that no option can been selected to display any vital on the monitor screen
            if (tobaccoCheck.isSelected() == false && bloodCheck.isSelected() == false && cholesterolCheck.isSelected() == false){
                JOptionPane.showMessageDialog(this, "No Options selected to monitor!", "Alert", JOptionPane.ERROR_MESSAGE);
            } else {
                // If the box or boxes are ticked, the respective observers are registered to receive new updates hourly
                registerObservers();
                // The monitor is displayed with the respective vitals
                displayMonitorGraph();
            }
          
        } catch (ParseException ex) {
            Logger.getLogger(MonitorScreen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_initiateMonitorActionPerformed

    private void closeMonitorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeMonitorActionPerformed
        // When the practitioner decided to stop monitoring the patient completely
        // the observers being monitored are unregistered and the frame is closed
        unregisterObservers();
        this.dispose();
    }//GEN-LAST:event_closeMonitorActionPerformed

    private void cholesterolCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cholesterolCheckActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cholesterolCheckActionPerformed

    private void tobaccoCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tobaccoCheckActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tobaccoCheckActionPerformed

    private void bloodCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bloodCheckActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bloodCheckActionPerformed

    private void resetMonitorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetMonitorActionPerformed
        // This is to erase all currently monitored data to allow the practitioner to 
        // re choose other combinations of vitals to monitor, it also un registers the observers
        // to ensure no unnecessary checks are made to the FHIR server reducing traffic
        // all the check boxes are unchecked
        unregisterObservers();        
        cholesterolCheck.setSelected(false);
        bloodCheck.setSelected(false);
        tobaccoCheck.setSelected(false);
        graphPanel.removeAll();
        graphPanel.updateUI();
        tobaccoStatus.setText("Unavailable");
    }//GEN-LAST:event_resetMonitorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MonitorScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MonitorScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MonitorScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MonitorScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

    }
    
    private final Patient patient;
    private TimeSeries cholesterolSeries;
    private TimeSeries diastolicSeries;
    private TimeSeries systolicSeries;
    private JFreeChart monitorChart;
    private HashMap<Double, String> patientcholestrol = new HashMap<>();
    private String patientTobacco;
    private LinkedHashMap<String, ArrayList<Double>> patientBloodPressure = new LinkedHashMap<>();
    private final ArrayList<Double> cholestrolValue = new ArrayList<>();
    private final ArrayList<String> datemeasured = new ArrayList<>();
    private final ArrayList<String> BPDates = new ArrayList<>();
    private final ArrayList<Double> diastolic = new ArrayList<>();
    private final ArrayList<Double> systolic = new ArrayList<>();
    private String patientname;
    private String patientid;
    private String patientage;
    private String patientgender;
    private CholestrolObserver cholesterolObserver;
    private BloodPressureObserver bloodPressureObserver;
    private TobaccoObserver tobaccoObserver;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel GenderLabel;
    private javax.swing.JLabel IDLabel;
    private javax.swing.JLabel MonitorHeading;
    private javax.swing.JPanel MonitorOption;
    private javax.swing.JPanel PatientInfo;
    private javax.swing.JLabel ageLabel;
    private javax.swing.JCheckBox bloodCheck;
    private javax.swing.JCheckBox cholesterolCheck;
    private javax.swing.JButton closeMonitor;
    private javax.swing.JPanel graphPanel;
    private javax.swing.JButton initiateMonitor;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel patientAge;
    private javax.swing.JLabel patientGender;
    private javax.swing.JLabel patientID;
    private javax.swing.JLabel patientName;
    private javax.swing.JButton resetMonitor;
    private javax.swing.JCheckBox tobaccoCheck;
    private javax.swing.JLabel tobaccoStatus;
    private javax.swing.JLabel tobaccoStatusLabel;
    // End of variables declaration//GEN-END:variables
}

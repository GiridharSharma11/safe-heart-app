/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Entities.Practitioner;
import StatisticalUpdate.PatientVitalsSubject;
import ServerData.PractitionerData;
import ServerData.UserData;
import com.mycompany.safeheartapp.AppLogo;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class LoginPage extends javax.swing.JFrame {

    /**
     * Creates new form LoginPage
     * @throws java.lang.Exception
     */
    
    // The subject instance which is passed from the main class
    private final PatientVitalsSubject passedSubject;
    
    public LoginPage(PatientVitalsSubject subject) throws Exception {
        initComponents();
        // New instance of the logo class is created
        AppLogo logo = new AppLogo();
        // The logo is embedded into the frame acting as the application logo
        logo.setAppIcon(this);
        // size of the frame is defined
        this.setSize(900, 560);
        this.passedSubject = subject;
        
    }    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainTitle = new javax.swing.JLabel();
        LogInBox = new javax.swing.JPanel();
        BoxHeading = new javax.swing.JPanel();
        LoginText = new javax.swing.JLabel();
        BoxBody = new javax.swing.JPanel();
        LoginButton = new javax.swing.JButton();
        LockIcon = new javax.swing.JPanel();
        lockImage = new javax.swing.JLabel();
        textUserID = new java.awt.TextField();
        SheildIcon = new javax.swing.JLabel();
        Wallpaper = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LoginPage");
        getContentPane().setLayout(null);

        mainTitle.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        mainTitle.setForeground(new java.awt.Color(255, 51, 51));
        mainTitle.setText("Safe ❣ Heart");
        getContentPane().add(mainTitle);
        mainTitle.setBounds(530, 30, 300, 70);

        LogInBox.setBackground(new java.awt.Color(255, 255, 255));

        BoxHeading.setBackground(new java.awt.Color(153, 0, 204));
        BoxHeading.setName("User Login"); // NOI18N

        LoginText.setBackground(new java.awt.Color(255, 255, 255));
        LoginText.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        LoginText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LoginText.setText("User Login");

        javax.swing.GroupLayout BoxHeadingLayout = new javax.swing.GroupLayout(BoxHeading);
        BoxHeading.setLayout(BoxHeadingLayout);
        BoxHeadingLayout.setHorizontalGroup(
            BoxHeadingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LoginText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        BoxHeadingLayout.setVerticalGroup(
            BoxHeadingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BoxHeadingLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(LoginText, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        BoxBody.setBackground(new java.awt.Color(255, 255, 51));

        LoginButton.setText("LOGIN");
        LoginButton.setBorderPainted(false);
        LoginButton.setContentAreaFilled(false);
        LoginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout BoxBodyLayout = new javax.swing.GroupLayout(BoxBody);
        BoxBody.setLayout(BoxBodyLayout);
        BoxBodyLayout.setHorizontalGroup(
            BoxBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(LoginButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        BoxBodyLayout.setVerticalGroup(
            BoxBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BoxBodyLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(LoginButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        lockImage.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lockImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/passcodeIcon.png"))); // NOI18N

        javax.swing.GroupLayout LockIconLayout = new javax.swing.GroupLayout(LockIcon);
        LockIcon.setLayout(LockIconLayout);
        LockIconLayout.setHorizontalGroup(
            LockIconLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LockIconLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lockImage, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        LockIconLayout.setVerticalGroup(
            LockIconLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LockIconLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lockImage)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        textUserID.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        textUserID.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        textUserID.setForeground(new java.awt.Color(153, 153, 153));
        textUserID.setText("User ID");
        textUserID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                textUserIDFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                textUserIDFocusLost(evt);
            }
        });
        textUserID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textUserIDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout LogInBoxLayout = new javax.swing.GroupLayout(LogInBox);
        LogInBox.setLayout(LogInBoxLayout);
        LogInBoxLayout.setHorizontalGroup(
            LogInBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(BoxHeading, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(LogInBoxLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(LogInBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BoxBody, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(LogInBoxLayout.createSequentialGroup()
                        .addComponent(LockIcon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textUserID, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        LogInBoxLayout.setVerticalGroup(
            LogInBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LogInBoxLayout.createSequentialGroup()
                .addComponent(BoxHeading, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(LogInBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LockIcon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(textUserID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BoxBody, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(LogInBox);
        LogInBox.setBounds(560, 290, 240, 160);

        SheildIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginLogo.png"))); // NOI18N
        getContentPane().add(SheildIcon);
        SheildIcon.setBounds(650, 230, 60, 60);

        Wallpaper.setIcon(new javax.swing.ImageIcon(getClass().getResource("/loginBackground.jpg"))); // NOI18N
        getContentPane().add(Wallpaper);
        Wallpaper.setBounds(0, 0, 900, 560);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void LoginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginButtonActionPerformed
           
        String practitionerID = textUserID.getText();
        
        // If there exists a user input entered
        boolean validInput = practitionerID.chars().allMatch(Character::isLetter);
                
            // The first check is made to see if the userinput is a valid clinician ID
            if (!validInput){
        
                try {
        
                    textUserID.setText("Logging in...");
                    // creating instances of classes for getting information about the clinician logging in to the system
                    UserData userdata = new UserData();
                    PractitionerData practitionerdata = new PractitionerData();
                    
                    // The name of the practitioner is retrieved
                    String name = userdata.getUserName("Practitioner", practitionerID);
                    // The patients of a practitioner are retrieved in an ArrayList
                    ArrayList<String> patientList = practitionerdata.patientList(practitionerID);  
                    // A new practitioner instance is created by finding its respective information
                    Practitioner practitioner = new Practitioner(name, practitionerID, patientList);
                    
                    // The home page is created and the practitioner instance is passed through
                    new HomePage(practitioner, passedSubject).setVisible(true);
                    
                    // The login page is shut down and the home page is then generated
                    this.setVisible(false);
                    this.dispose();
                    
                } catch (Exception ex) {
                    Logger.getLogger(LoginPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                // If a invalid ID was entered, the clinician is prompted to enter the ID again
                textUserID.setText("Re-enter ID");
            }
   
    }//GEN-LAST:event_LoginButtonActionPerformed

    
  
    private void textUserIDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textUserIDFocusGained
        textUserID.setText("");
    }//GEN-LAST:event_textUserIDFocusGained

    private void textUserIDFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_textUserIDFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_textUserIDFocusLost

    private void textUserIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textUserIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textUserIDActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

       
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BoxBody;
    private javax.swing.JPanel BoxHeading;
    private javax.swing.JPanel LockIcon;
    private javax.swing.JPanel LogInBox;
    private javax.swing.JButton LoginButton;
    private javax.swing.JLabel LoginText;
    private javax.swing.JLabel SheildIcon;
    private javax.swing.JLabel Wallpaper;
    private javax.swing.JLabel lockImage;
    private javax.swing.JLabel mainTitle;
    private java.awt.TextField textUserID;
    // End of variables declaration//GEN-END:variables

}

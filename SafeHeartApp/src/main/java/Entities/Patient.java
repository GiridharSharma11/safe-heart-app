/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class Patient extends User{
 
    /**
     * Class variables initialized to store additional patient info which has not been specified in the parent class
     */
    private HashMap<Double, String> cholestrolLevels = new HashMap<>();
    private String patientAge;
    private LinkedHashMap<String, ArrayList<Double>> bloodPressure = new LinkedHashMap<>();
    private String tobaccoUse;
    private String gender;
    
    public Patient(String fullName, String userID, String gender, HashMap<Double, String> cholestrolList, String age, LinkedHashMap<String, ArrayList<Double>> bloodPressureValues, String tobacco) {
        super(fullName, userID);
        this.gender = gender;
        this.cholestrolLevels = cholestrolList;
        this.patientAge = age;
        this.bloodPressure = bloodPressureValues;
        this.tobaccoUse = tobacco;
    }
 
    /**
     * This function sets the cholesterol values of a patient to a particular patient 
     * @param cholestrolList 
     */
    public void setCholestrol(HashMap<Double, String> cholestrolList){
        this.cholestrolLevels = cholestrolList;
    }
    
    /**
     * This function retrieves all the cholesterol of a singular patient in a HashMap
     * @return cholesterol values of a patient
     */
    public HashMap<Double, String> getCholestrol(){
        return this.cholestrolLevels;
    }
    
    /**
     * This function sets the age of a patient
     * @param age 
     */
    public void setAge(String age){
        this.patientAge = age;
    }
    
    /**
     * This function gets the age of a patient
     * @return patient age
     */
    public String getAge(){
        return this.patientAge;
    }

    /**
     * This function sets the blood pressure values of a patient along with 
     * the dates they were measured at
     * @param bloodPressureValues 
     */
    public void setBloodPressure(LinkedHashMap<String, ArrayList<Double>> bloodPressureValues){
        this.bloodPressure = bloodPressureValues;
    }
    
    /**
     * This function retrieves all the blood pressure values along with their date measures
     * for a particular patient
     * @return blood pressure values with their effective dates
     */
    public LinkedHashMap<String, ArrayList<Double>> getBloodPressure(){
        return this.bloodPressure;
    }
    
    /**
     * This function sets the tobacco use status for a particular patient
     * @param tobacco 
     */
    public void setTobaccoUse(String tobacco){
        this.tobaccoUse = tobacco;
    }
    
    /**
     * This functions gets the latest tobacco use status for a patient
     * @return tobacco status latest
     */
    public String getTobaccoUse(){
        return this.tobaccoUse;
    }
    
    /**
     * This function sets the gender of a patient
     * @param gender 
     */
    public void setGender(String gender){
        this.gender = gender;
    }
    
    /**
     * This function gets the gender of a particular patient
     * @return gender of a patient
     */
    public String getGender(){
        return gender;
    }
}

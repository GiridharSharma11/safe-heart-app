/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.ArrayList;

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal Sharma
 */
public class Practitioner extends User {
    
    /**
     * Initializing class variables and constructor
     */
    private ArrayList<String> patientlist = new ArrayList<>();
    // Additional patList is present which stores the patients for a clinician in a list
    public Practitioner(String fullName, String userID, ArrayList<String> patList) {
        super(fullName, userID);
        this.patientlist = patList;
    }
    
    /**
     * This function sets the clinicians patients in a list
     * @param patList 
     */
    public void setPatients(ArrayList<String> patList){
        this.patientlist = patList;
    }
    
    /**
     * This function gets the clinicians patients in a list form
     * @return 
     */
    public ArrayList<String> getPatients(){
        return patientlist;
    }
   
    
}

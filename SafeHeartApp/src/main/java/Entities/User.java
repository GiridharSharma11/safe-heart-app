package Entities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Siddhant Dube
 * @author Giridhar Gopal
 */
public abstract class User {
    
    /**
     * Initializing class variables
     */
    private String fullName, userID;
 
    // Class constructor
    public User(String fullName, String userID) {
        setValues(fullName, userID);
    }    

    /**
     * This function allows to set the personal info for the users
     * @param fullName
     * @param userID
     * @param gender 
     */
    private void setValues(String fullName, String userID) {
        setfullName(fullName);
        setuserID(userID);
    }

    /**
     * getting the full name of a user
     * @return fullName : String
     */
    public String getfullName() {
        return fullName;
    }
    
    /**
     * Getting the ID of a user
     * @return String; userID
     */
    public String getuserID() {
        return userID;
    }
    
    
    /**
     * Setting the full name of a user
     * @param fullName 
     */
    public void setfullName(String fullName) {
        this.fullName = fullName;
    }
    
    /**
     * Setting the userID of the user
     * @param userID 
     */
    public void setuserID(String userID) {
        this.userID = userID;
    }
    
}
